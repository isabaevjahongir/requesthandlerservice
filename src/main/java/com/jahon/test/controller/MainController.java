package com.jahon.test.controller;

import com.jahon.test.HmacShaEncoder;
import com.jahon.test.Properties;
import com.jahon.test.ResponseTemplates;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.security.NoSuchAlgorithmException;
import java.util.Comparator;
import java.util.Map;
import java.util.TreeMap;


/**
 * Основной контроллер приложения
 *
 * @author jahongirka
 **/
@RestController
public class MainController {
    @Autowired
    private Properties props;


    @PostMapping("/{operationId}")
    public String handle(@PathVariable String operationId, @RequestParam Map<String, String> allParams) throws NoSuchAlgorithmException {
        String sortedParams = handleParam(allParams);

        String encodedParams = HmacShaEncoder.hmacSha256(sortedParams, props.getEncriptionKey());

        String result = String.format(ResponseTemplates.SUCCESS_RESPONSE, encodedParams);

        return result;
    }

    private String handleParam(Map<String, String> allParams) {
        StringBuilder sb = new StringBuilder();

        TreeMap<String, String> sortedMap = new TreeMap<>(Comparator.naturalOrder());
        sortedMap.putAll(allParams);

        for (Map.Entry entry : sortedMap.entrySet()) {
            sb.append(entry.getKey()).append("=").append(entry.getValue()).append(" ");
        }

        return sb.toString().trim().replace(" ", "&");
    }

}
