package com.jahon.test.controller;

import com.jahon.test.Properties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * Перехватчик звпросов контроллера
 *
 * @author jahongirka
 * @see HandlerInterceptor
 **/
@Component
public class TokenInterceptor implements HandlerInterceptor {

    @Autowired
    private Properties props;

    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o) throws Exception {
        String token = httpServletRequest.getHeader("Token");
        String appToken = props.getAppToken();

        if (token == null || !token.equals(appToken)) {
            httpServletResponse.setStatus(403);
            return false;
        }

        return true;
    }
}
