package com.jahon.test;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Класс получение настроек из файла-настроек
 *
 * @author jahongirka
 **/
@Component
public class Properties {
    @Value("${token}")
    private String appToken;

    @Value("${key}")
    private String encriptionKey;

    public String getAppToken() {
        return appToken;
    }

    public String getEncriptionKey() {
        return encriptionKey;
    }
}
