package com.jahon.test;


/**
 * Данный интерфейс используется для хранения шаблонов ответов на запросы
 *
 * @author jahongirka
 **/
public interface ResponseTemplates {
    String SUCCESS_RESPONSE = "{\"status\" : \"success\", \n \"result\": [" +
            "{\"signature\": \"%s\"}, ] \n}";
}
