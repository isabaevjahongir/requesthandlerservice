package com.jahon.test;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

/**
 * Класс для получения Hmac SHA256 строки
 * В качестве ключа алгоритма, используется статическая переменная SECRET_KEY
 *
 * @author jahongirka
 **/

public class HmacShaEncoder {
    public static final String SHA_TYPE = "HmacSHA256";
    public static final Charset ENCODING = StandardCharsets.UTF_8;

    /**
     * @param value - строка для хеширования
     * @return - хешированная строка. Ключ алгоритма это переменная SECRET_KEY
     **/
    public static String hmacSha256(String value, String key) {

        try {
            SecretKeySpec signingKey = new SecretKeySpec(key.getBytes(ENCODING), SHA_TYPE);
            Mac mac = Mac.getInstance(SHA_TYPE);
            mac.init(signingKey);

            byte[] rawHmac = mac.doFinal(value.getBytes(ENCODING));

            byte[] hexArray = {(byte) '0', (byte) '1', (byte) '2', (byte) '3', (byte) '4', (byte) '5', (byte) '6', (byte) '7', (byte) '8', (byte) '9', (byte) 'a', (byte) 'b', (byte) 'c', (byte) 'd', (byte) 'e', (byte) 'f'};
            byte[] hexChars = new byte[rawHmac.length * 2];

            for (int j = 0; j < rawHmac.length; j++) {
                int v = rawHmac[j] & 0xFF;
                hexChars[j * 2] = hexArray[v >>> 4];
                hexChars[j * 2 + 1] = hexArray[v & 0x0F];
            }
            return new String(hexChars);
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }

    }

}
